#!/bin/bash

if [ -n "$APPLICATION_CONF" ]; then
    echo "Copying a file /opt/cerebro/conf/application.conf from APPLICATION_CONF environment variable"
    mkdir -p /opt/cerebro/conf
    echo "$APPLICATION_CONF" > /opt/cerebro/conf/application.conf
fi

exec "/opt/cerebro/bin/cerebro" "$@"
